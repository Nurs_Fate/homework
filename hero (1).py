class SuperHero:
    people = 'people'

    def __init__(self, name, nickname, superpower, health_points, catchphrase):
        self.name = name
        self.nickname = nickname
        self.superpower = superpower
        self.health_points = health_points
        self.catchphrase = catchphrase

    def name1(self):
        print(f"__________________\n"
              f"Name: {self.name}")

    def hp(self):
        print(f"HP: {self.health_points * 2}")

    def __str__(self):
        return f"Nickname: {self.nickname}\n" \
               f"Superpower: {self.superpower}\n" \
               f"HP: {self.health_points}\n" \
               f"Catchphrase: {len(self.catchphrase)}"

statistics = SuperHero("Брюс Уэйн", "Бэтмен", "Деньги", 100, "Я Бэтмен")
statistics.name1()
statistics.hp()
print(statistics)


class AirHero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage):
        super().__init__(name, nickname, superpower, health_points, catchphrase)
        self.damage = damage
        self.fly = False

    def __str__(self):
        return f"{super().__str__()}\n" \
               f"Damage: {self.damage}"

    def hp(self):
        super().hp()
        print(f"HP: {self.health_points ** 2}")
        self.fly = True

    def fly_phrase(self):
        if self.fly:
            print("Fly in the sky!")

statistics2 = AirHero("Кларк", "Супермен", "Летать", 1000, "Знаешь, я словно живу в мире, где всё создано из хрусталя", 10000)
statistics2.name1()
statistics2.hp()
statistics2.fly_phrase()
print(statistics2)

class EarthHero(AirHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage):
        super().__init__(name, nickname, superpower, health_points, catchphrase, damage)

statistics3 = AirHero("Барри", "Супермен", "Флеш", 100, "я самый быстрый", 100)
statistics3.name1()
statistics3.hp()
statistics3.fly_phrase()
print(statistics3)

class Villain(EarthHero):
    people = 'monster'

    def gen_x(self):
        pass

# deactivate
# venv\Scripts\activate
# pip install tprint colorama
# pip install -r regu.txt
# pip install -r имя модуля
# python -m venv env
# pip freeze
# pip freeze > reg.txt

# import art
# print(art.tprint("( ^ _ ^ )"))

import sqlite3

db = sqlite3.connect("")




