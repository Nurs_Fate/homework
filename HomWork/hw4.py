class Name:
    def __init__(self, name):
        self.name = name


class Age:
    def __init__(self, age):
        self.__age = age

    @property  # property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        if age < 0:
            raise ValueError('Корректный возраст')
        self.__age = age


class Acquaintance:
    def greeting(self):
        return f"Hi {self}"


class Growth:
    def growth(self):
        return f"I'm {self} years old"


class Adios(Name, Age, Acquaintance, Growth):
    def __init__(self, name, age):
        Name.__init__(self, name)
        Age.__init__(self, age)


object = Adios('Nursultan', 18)
object.greeting()
object.growth()

print(f'Имя: {object.name}')
print(f'Возрост: {object.age}')
# print(f'Сколько тебе лет: {object.growth()}')
# print(f'Здороваться: {object.greeting()}')
