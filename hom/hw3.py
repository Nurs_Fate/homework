
class Bank:
    def __init__(self, name, balance):
        self._name = name
        self._balance = balance

    def moneyX(self):
        addition = int(input("Сумма пополнение:"))
        # self._balance + addition
        return addition + self._balance

    def kill(self):
        self._balance = 0
        return self._balance

    def __jackpot(self):
        self._balance *= 10
        return self._balance * 10

    def __plus(self, client):
        if client._balance:
            self._balance += client._balance
            return self._balance


client1 = Bank('Гон', 10000)
client2 = Bank('Килуа', 10000)

print(f'add money: {client1.moneyX()}')
print(f'После обнуление: {client1.kill()}')
print(f'Jackpot: {client1._Bank__jackpot()}')
print(f'1+1: {client1._Bank__plus(client2)}')